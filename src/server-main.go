package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

type Poll struct {
	Id       int
	Chars    []Character
	BestChar int
}

type Character struct {
	Id        int
	Givenname string
	Surname   string
}

func newPoll(id, bestChar int, chars []Character) Poll {
	return Poll{id, chars, bestChar}
}

func newCharacter(id int, givenname, surname string) Character {
	return Character{id, givenname, surname}
}

func getCharsForPoll(pollId int) []Character {
	return []Character{ //TODO replace with db query
		newCharacter(1, "Yozora", "Mikazuki"),
		newCharacter(2, "Sena", "Kashiwazaki"),
		newCharacter(3, "Yukimura", "Kusunoki"),
		newCharacter(4, "Rika", "Shiguma"),
	}
}

func getBestCharIdForPoll(pollId int) int {
	return 1 //TODO replace with db query
}

func PollHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("PollHandler got matching req")
	if strings.Compare(r.Method, "GET") == 0 {
		id, _ := strconv.Atoi(mux.Vars(r)["id"])
		poll, _ := json.Marshal(newPoll(123, 1, getCharsForPoll(id))) //TODO replace with db query
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte(poll))
	} else if strings.Compare(r.Method, "POST") == 0 {
		log.Println("Creating new poll")
	}
}

func CharsHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("CharsHandler got matching req")
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	chars, _ := json.Marshal(getCharsForPoll(id))
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte(chars))
}

func BestCharHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("BestCharHandler got matching req")
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	bestChar, _ := json.Marshal(getBestCharIdForPoll(id))
	w.Header().Add("Content-Type", "application/json")
	w.Write([]byte(bestChar))
}

func main() {
	log.Println("Starting server...")

	r := mux.NewRouter()
	r.PathPrefix("/api/poll/{id}/chars").HandlerFunc(CharsHandler)
	r.PathPrefix("/api/poll/{id}/bestchar").HandlerFunc(BestCharHandler)
	r.PathPrefix("/api/poll/{id}").HandlerFunc(PollHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./assets/")))

	http.ListenAndServe(":8080", r)
}
