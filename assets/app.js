angular.module('bcpApp.services', []);
angular.module('bcpApp.controllers', ['ngRoute']);
angular.module('bcpApp.directives', []);

angular.module('bcpApp', ['bcpApp.services', 'bcpApp.controllers', 'bcpApp.directives', 'ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/poll/:pollId', {
		templateUrl: 'partials/poll.html', 
		controller: 'PollCtrl'
	})
	.when('/admin/:pollId', {
		templateUrl: 'partials/admin.html', 
		controller: 'PollAdminCtrl'
	})
	.when('/results/:pollId', {
		templateUrl: 'partials/results.html', 
		controller: 'PollResultsCtrl'
	})
	.otherwise({
		redirectTo: '/poll/1'
	})
	;
}]);