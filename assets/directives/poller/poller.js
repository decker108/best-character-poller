angular.module('bcpApp.directives')
.directive('poller', ['$interval', '$timeout', '$routeParams', 'PollService', 
($interval, $timeout, $routeParams, PollService) => {
	return {
    	restrict: 'E',
    	templateUrl: 'directives/poller/poller.html',
    	scope: {
    		nameOrder: '='
    	},
    	controller: ($scope, $routeParams, PollService) => {
    		$scope.pollId = $routeParams.pollId;

			$scope.chars = PollService.getPollChars($scope.pollId);

			var bestCharId = PollService.getBestCharId($scope.pollId);
			$scope.selectedCharId = 0;

			$scope.getCharName = (char) => {
				if (!$scope.nameOrder) {
					return char.givenname + " " + char.surname;
				} else {
					return char.surname + " " + char.givenname;
				}
			}
    	},
    	link: (scope, element, attrs) => {
			element.bind('click', () => {
            	var radioButtons = element.find("input") || [];
            	$timeout((charId) => {
            		console.log("Running timeout fn");
	            	for (var i = radioButtons.length - 1; i >= 0; i--) {
            			if (Number(radioButtons[i].attributes['charid'].value) === charId) {
            				radioButtons[i].checked = true;
            			} else {
            				radioButtons[i].checked = false;
            			}
	            	};
            	}, 300, true, PollService.getBestCharId($routeParams.pollId));
	        })
    	}
	}
}]);
