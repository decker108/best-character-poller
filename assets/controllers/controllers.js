angular.module('bcpApp.controllers')
.controller('PollCtrl', ['$scope', 'PollService', 
function($scope, PollService) {
	$scope.asianStyleNameOrdering = false;
	$scope.toggleNameOrder = () => {
		$scope.asianStyleNameOrdering = !$scope.asianStyleNameOrdering;
	}
}]);
