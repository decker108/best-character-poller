angular.module('bcpApp.services')
.factory('PollService', function() {
	return {
		getPollChars: (pollId) => {
			return [
				{id:1, givenname:"Yozora", surname:"Mikadzuki", selected: false},
				{id:2, givenname:"Sena", surname:"Kashiwazaki", selected: false},
				{id:3, givenname:"Yukimura", surname:"Kusunoki", selected: false},
				{id:4, givenname:"Rika", surname:"Shiguma", selected: false}
			];
		},
		getBestCharId: (pollId) => {
			return 1;
		} 
	}
});
